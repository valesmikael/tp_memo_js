
class App{

    constructor(){
        // on récupère tous les champs du formulaire        
        this.rdv_form = document.getElementById('Rdv_form');
        this.titre = document.getElementById('titre');
        this.contenu = document.getElementById('contenu');
        this.debut = document.getElementById('debut')
        this.fin = document.getElementById('fin')
        this.submit_Rdv = document.getElementById('submit_Rdv');
        this.container = document.getElementById('container');

        this.edit_index = -1;

    }

    //on vérifie si le formulaire et vide
    formIsEmpty(){

        return this.titre.value == '' || this.contenu.value == '';
        
    }

    //on récupère tous les index par élément du tableau
    getIndex( item, items ){

        for( let index in items){

            if ( items[index] == item ) return index

        }

        return -1;

    }

    //mode de modification du memo, champs à modifier
    editMode( memo, index ){
        //this.submit_Rdv.value = 'Modifier';
        this.edit_index = index;
        this.titre.value = memo.titre;
        this.debut.value = memo.debut;
        this.fin.value = memo.fin;
        this.contenu.value = memo.contenu;
        
    }

    //edition de la modification
    isEditMode(){
        return this.edit_index > -1;
    }

    //mode creation
    createMode( index ){
        this.edit_index = -1;
        this.submit_Rdv.value = 'Enregistrer';
        this.rdv_form.reset();
    }


}




export default new App;