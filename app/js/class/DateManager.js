
import { Rdv } from '../class/Rdv';
// class qui permet de faire les comparatifs de date
export class DateManager{

    //dans le constructeur on importe let dateM = new DateManager( this.debut ) qui est donc la valeur que prend 'datestr'
    constructor( datestr ){

        this.today = new Date;
        this.today.setUTCHours(0,0,0,0);
        this.todaytime = this.today.getTime();//transformation de la date en string

        this.date = new Date(datestr);
        this.datetime = this.date.getTime();

        this.DOM=null;


    }
    

    green() {
        // si la date du jour et égale à la date de l'évènement
         if (this.todaytime == this.datetime){

            return true;

         }
              
    }

    red() {

        // si la date du jour depasse date de l'évènement
        if (this.todaytime > this.datetime){

            return true;
         }
         
        
    }

    blue() {

        // si la date du jour arrive à 3 jours avant la date de l'évènement
        if (this.todaytime == (this.datetime-259200000)){

            return true;
        // si la date du jour arrive à 2 jours avant la date de l'évènement
         }else if (this.todaytime == (this.datetime-172800000)){

                return true;
        // si la date du jour arrive à 1 jours avant la date de l'évènement
         }else if (this.todaytime == (this.datetime-86400000)){

            return true;

     }
        
        
    }
   
}
