
import memoTemplate from '../../templates/memo';
import { DateManager } from './DateManager';

export class Rdv{
    // On construit les donnée(contenu) que l'on veut assimiler à un memo
    // On lui précise que les élément du DOM(contenu de la page sont à null au départ)
    // Nous en avons besoin pour sauvegarder les info dans save() 
    constructor(titre, contenu, debut, fin){
        this.titre = titre;
        this.contenu = contenu;
        this.debut = debut;
        this.fin = fin;
        this.color = null;
        this.DOM = null;
        this.DOMTitre = null;
        this.DOMContenu = null;
        this.DOMDebut = null;
        this.DOMFin = null;
        this.DOMButton = null;
        this.DOMDetail = null;            
        
    }

    // Construction des élément qui seront modifier dans le container
    renderTemplate( container ){

        
        
        let div = document.createElement('div');
        div.innerHTML = memoTemplate;

        this.DOM = div.firstChild; 
        this.DOMTitre = this.DOM.getElementsByTagName('h4')[0];
        this.DOMContenu = this.DOM.getElementsByTagName('p')[0];
        this.DOMDebut = this.DOM.getElementsByClassName('debut')[0];
        this.DOMFin = this.DOM.getElementsByClassName('fin')[0];
        this.DOMOpen = this.DOM.getElementsByClassName('Open')[0];
        this.DOMClose = this.DOM.getElementsByClassName('Close')[0];
        this.DOMDetail = this.DOM.getElementsByClassName('Details')[0];
        
        

        //changer les couleur selon la date du debut de rdv et la date du jour 
        let dateM = new DateManager( this.debut );

        if( dateM.red() ) {
            
            this.DOM.style.background = '#FF4B4B';
            this.DOM.style.border = 'solid 2px #CC0033';
            this.color = "red";
            
            
        }
        else if( dateM.green() ){

            this.DOM.style.background = '#BBFFA2';
            this.DOM.style.border = 'solid 2px #00CC00';
                        

        }else if( dateM.blue() ){

            this.DOM.style.background = '#B8E9FF';
            this.DOM.style.border = 'solid 2px #3366FF';

        }else{

            this.DOM.style.background = '##ebe5e5';
            this.DOM.style.border = 'solid 2px #1af3cf';

        };
        //ouverture et fermeture des details de memo
        
        this.DOMOpen.addEventListener('click', () => {
        
            this.DOMDetail.style.display = 'block';

        });

        this.DOMClose.addEventListener('click', () => {
        
            this.DOMDetail.style.display = 'none';

        });
        
        
    
    // on envoi les élément qui seront rempli dans une function qui les rempliera.(ici reRender())
        this.reRender();

        container.appendChild( this.DOM );

        
    }

    //action d'intégration des valeurs
    reRender(){
        //console.log( this );
        this.DOMTitre.innerText = this.titre;
        this.DOMContenu.innerText = this.contenu;
        this.DOMDebut.value = this.debut;
        this.DOMFin.value = this.fin;

                
    }
    
    clear(){
        this.DOM.remove();

    }

       

}