import * as style from '../scss/styles';// import du fichier styles
import app from'./class/App';
import memoService from './services/MemoService';
import { Rdv } from'./class/Rdv';
//import datemanager from'./class/DateManager';



// Html document est pret, on envoi les donnée du post pour affichage
document.addEventListener('DOMContentLoaded', function(){

     
    memoService.renderAll( app.container );
    memoService.message();
   
    

})




//action au clic sur supprimer 
app.container.addEventListener('click', function(event){


    let target = event.target;// dés qu'il y a un evenement

    //si supprimer
    if( target.classList.contains('supprimer') ){// dès qu'on click sur supprimer


        let parent = target.parentNode;// on cible le parent de l'élément cliqué
        let DOMMemos = document.getElementsByClassName(' memo ');// on cible le type d'élément associé ici c'est la div 'memo'
        let index = app.getIndex (parent, DOMMemos );// et l'index associé
        memoService.remove( index );// on supprime l'élément 

        } 

        //sinon si on modifie
        else if( target.classList.contains('modifier') ){// dès qu'on click sur modifier
    
            let parent = target.parentNode;// on cible le parent de l'élément cliqué
            let DOMMemos = document.getElementsByClassName(' memo ');// on cible le type d'élément associé ici c'est la div 'memo'
            let index = app.getIndex (parent, DOMMemos );// et l'index associé
    
            let memo = memoService.memos[index];// on associe les valeurs récupérer au champs à modifier
            app.titre.value = memo.titre;
            app.contenu.value = memo.contenu;
            app.debut.value = memo.debut;
            app.fin.value = memo.fin;
    
            app.editMode( memo, index );
    
        }
              
    else{
        app.createMode();
    }



});

// on ecoute si il y a une action sur le bouton submit du formulaire de base
app.rdv_form.addEventListener('submit', function( event){
//par défault on lui dit qu'il n'y a pas d'action
    event.preventDefault();

//si le formulaire et vide, on return rien
    if( app.formIsEmpty()) return;
//si qu'on est pas en mode modification
    if( !app.isEditMode()){
        console.log(app);

//on construit le memo à enregistrer et à afficher
    let memo = new Rdv(
        app.titre.value,
        app.contenu.value,
        app.debut.value,
        app.fin.value
    );

//on sauvegarde le nouveau memo et on envoi le rendu  
        memo.renderTemplate( app.container );
        memoService.save( memo );
//on remet le formulaire à zéro
    app.rdv_form.reset();

    } 

    //alors on enregistre le memo comme nouveau memo
    else{
        console.log(app);
        let memo = memoService.memos[ app.edit_index ];
        memo.titre = app.titre.value;
        memo.contenu = app.contenu.value;
        memo.debut = app.debut.value;
        memo.fin = app.fin.value;
        console.log(memo);
        memoService.update( memo );
        app.createMode();

    }


    });




  



    



    
