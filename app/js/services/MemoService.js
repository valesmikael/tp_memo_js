import { Rdv } from '../class/Rdv';



class MemoService{

 constructor(){
    this.memos = [];// va acceuillir les données enregistrées au moment du localStorage
    this.all();
    
    //console.log(this.memos);    
}  

//sauvegarde un memo et le pousse dans this.memos
save( memo ) {

  
    this.memos.push( memo );
    this.saveAll();
    // JSON.stringify converti un objet JS en string JSON
    

}

//sauvegarde de tous les memos
saveAll(){
    // JSON.stringify converti un objet JS en string JSON
    // on remet à jour
    localStorage.setItem('memos', JSON.stringify( this.memos ));

}

//On récupère les élément du localStorage
all(){

    let memos = localStorage.getItem('memos');
    if( memos ){
        // JSON.parse converti une string JSON en Objet JS
        let json_memos = JSON.parse(memos); //console.log(json_memos);
        for( let json_memo of json_memos){

            let  memo = new Rdv(
                json_memo.titre,
                json_memo.contenu,
                json_memo.debut,
                json_memo.fin,

            );
            this.memos.push( memo );
        }
    }

}

//on injecte dans renderTemplate les notes récupérer dans le localStorage avec all().
renderAll( container ){
    
    for(let memo of this.memos ){
        memo.renderTemplate( container );
    }
}

remove( index ){

    let memo = this.memos[index];

    this.memos.splice(index, 1);
    this.saveAll();
    memo.clear(); // on remet à jour
    
}

// on rempli les champs modifier et on sauvegarde
update( memo ){

    memo.reRender();
    this.saveAll();
}

message(){
    

    this.intro = 'Evènement aujourd\'hui: ';
    this.att='';

    for(let memo of this.memos ){
        //console.log(memo);
        if (memo.color=="red"){

            this.att += this.intro + memo.titre + '\n';             
            //console.log(this.att);
        }
        
    }
  alert(this.att);
  return (this.att);

};


}


export default new MemoService;

