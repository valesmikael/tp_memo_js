const path = require('path');

let CONFIG = {

    mode: 'development',
    devtool: 'inline-source-map',
    entry: './app/js/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },

    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3000,
        watchContentBase: true,
        open: true
    },

    module: {
                rules: [
                            {           
                                test: /\.scss$/,
                                use: [
                                        'style-loader',
                                        {
                                            loader: 'css-loader',
                                            options:{
                                                sourceMap: true
                                            }
                                        },
                                        {
                                            loader: 'sass-loader',
                                            options:{
                                                sourceMap: true
                                            }  
                                        }                 
                                    
                                    ]
                            },
                            {
                                test:/\.html$/,
                                use: 'html-loader'
                            }
                        
                        ]
            },
    resolve: {
        extensions: ['.js', '.scss', '.html']
    }

}

module.exports = CONFIG;